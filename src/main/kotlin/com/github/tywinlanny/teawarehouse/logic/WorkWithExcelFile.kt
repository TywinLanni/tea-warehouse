package com.github.tywinlanny.teawarehouse.logic

import org.apache.poi.ss.usermodel.Row
import org.apache.poi.xssf.usermodel.XSSFWorkbook
import tornadofx.*
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream
import java.lang.NullPointerException


class WorkWithExcelFile {

    private val order = mutableListOf<TeaPosition>()
    var cost = 0.0
        private set

    fun setOrder(newOrder: MutableList<TeaPosition>){
        order.clear()
        cost = 0.0
        order.addAll(newOrder)
    }

    fun adaptationOptOrder(orderPath: String, sheetName: String = "Лист1"): MutableList<TeaPosition> {
        order.clear()
        cost = 0.0
        order.addAll(createTeaPositions(openExcelAndGetRows(orderPath, sheetName)))
        return order
    }

    fun refactorChinaPrice(warehousePath: String, orderName: String, sheetName: String = "Лист1"): Boolean {
        val notNullPositions = mutableMapOf<String, String>()
        val rows = openExcelAndGetRows(warehousePath, sheetName)
        while (rows.hasNext()){
            val currentRow = rows.next()
           if (currentRow.rowNum < 12 || currentRow.getCell(5)?.toString().isNullOrEmpty())
               continue
            notNullPositions[currentRow.getCell(0).toString()] = currentRow.getCell(5).toString()
        }
        val newFilePath = File("").absolutePath + "/" + "SBOR_$orderName"
        try {
            File(File("").absolutePath + "/" + "chineBlank/SBOR.xlsx").copyTo(File(newFilePath))
        } catch (e: FileAlreadyExistsException){
            warning("Файл $newFilePath уже создан")
            return false
        }
        val workbook = XSSFWorkbook(FileInputStream(File(newFilePath)))
        val sheet = workbook.getSheet(sheetName)
        val rowsChina: Iterator<Row>
        try {
            rowsChina = sheet.iterator()
        } catch (e: NullPointerException) {
            warning("В файле SBOR не обнаружен Лист1")
            return false
        }
        while (rowsChina.hasNext()) {
            val currentRow = rowsChina.next()
            if (currentRow.rowNum < 2 || currentRow.getCell(0)?.toString().isNullOrEmpty())
                continue
            if (notNullPositions.keys.contains(currentRow.getCell(0).toString()))
                currentRow.getCell(4).setCellValue(notNullPositions[currentRow.getCell(0).toString()])
        }
        val newExcelFile = FileOutputStream(newFilePath)
        workbook.write(newExcelFile)
        newExcelFile.close()
        refactorExcelFileToMyWarehouse(newFilePath, 4)
        return true
    }


    fun refactorExcelFileToMyWarehouse(warehousePath: String, cellNum: Int = 5, sheetName: String = "Лист1"){
        try {
            val rowsInNewFile = mutableListOf<MutableList<String>>()
            val workbook = XSSFWorkbook(FileInputStream(File(warehousePath)))
            val sheet = workbook.getSheet(sheetName)
            val newSheet = workbook.createSheet("Лист2")
            val rows = sheet.iterator()
            while (rows.hasNext()) {
                val currentRow = rows.next()
                if (currentRow.getCell(cellNum)?.toString().isNullOrEmpty()) {
                    continue
                }
                rowsInNewFile.add(mutableListOf(currentRow.getCell(0)?.toString() ?: "", currentRow.getCell(1)?.toString() ?: "",
                        currentRow.getCell(2)?.toString() ?: "", currentRow.getCell(3)?.toString() ?: "",
                        currentRow.getCell(4)?.toString() ?: "", currentRow.getCell(5)?.toString() ?: "",
                        currentRow.getCell(6)?.toString() ?: "", currentRow.getCell(7)?.toString() ?: ""))
            }
            removeExtraZeros(rowsInNewFile)
            for ((rowNum, row) in rowsInNewFile.withIndex()){
                val nextRow = newSheet.createRow(rowNum)
                for ((index, cellValue) in row.withIndex()){
                    nextRow.createCell(index).setCellValue(cellValue)
                }
            }
            if (cellNum == 4){
                val chinaRows = newSheet.iterator()
                var lastRowNum = 0
                var allSum = 0.0
                while (chinaRows.hasNext()){
                    lastRowNum ++
                    val currentRow = chinaRows.next()
                    if (currentRow.rowNum == 0)
                        continue
                    val cellValue = currentRow.getCell(4).stringCellValue.toDouble() * currentRow.getCell(3).stringCellValue.toDouble()
                    allSum += cellValue
                    currentRow.getCell(5).setCellValue(cellValue)
                }
                var lastRow = newSheet.getRow(lastRowNum)
                if (lastRow == null)
                    newSheet.createRow(lastRowNum)
                lastRow = newSheet.getRow(lastRowNum)
                lastRow.createCell(5)
                lastRow.getCell(5).setCellValue(allSum)
            }
            workbook.removeSheetAt(0)
            val newExcelFile = FileOutputStream(warehousePath)
            workbook.write(newExcelFile)
            newExcelFile.close()
        }catch (e: IllegalStateException){ }
    }

    private fun removeExtraZeros(emptyRowsIndex: MutableList<MutableList<String>>) {
        for (excelStringList in emptyRowsIndex.filter { it != emptyRowsIndex[0] }) {
            for ((index, _) in excelStringList.withIndex()) {
                if (index == 3 || index == 4 || index == 6) {
                    if (excelStringList[index].toDouble() - excelStringList[index].toDouble().toInt() !in 0.01..0.09)
                        excelStringList[index] = excelStringList[index].replace(".0", "")
                }
            }
        }
    }

    fun refactorExcelFile(warehousePath: String, mode: ModeWorkWithExcel, sheetName: String = "Лист1") {
        try {
            val workbook = XSSFWorkbook(FileInputStream(File(warehousePath)))
            val sheet = workbook.getSheet(sheetName)
            val rows = sheet.iterator()
            while (rows.hasNext()) {
                val currentRow = rows.next()
                if (currentRow.rowNum == 0)
                    continue
                if (currentRow.getCell(2)?.toString().isNullOrEmpty()) {
                    break
                }
                val art = currentRow.getCell(1)?.stringCellValue
                val oldValue = currentRow.getCell(5)?.numericCellValue
                if (!art.isNullOrEmpty() && checkOrder(art) && oldValue != null)
                    currentRow.getCell(5).setCellValue(getNewValue(oldValue, art, mode))
            }
            val newExcelFile = FileOutputStream(warehousePath)
            workbook.write(newExcelFile)
            newExcelFile.close()
        }catch (e: IllegalStateException){  }
    }

    private fun getNewValue(oldValue: Double, art: String, mode: ModeWorkWithExcel): Double {
        for (position in order) {
            if (position.art == art) {
                return when (mode) {
                    ModeWorkWithExcel.SUBTRACT -> oldValue - position.value
                    ModeWorkWithExcel.ADD -> oldValue + position.value
                }
            }
        }
        return 0.0
    }

    private fun checkOrder(positionName: String): Boolean {
        for (position in order) {
            if (position.art == positionName)
                return true
        }
        return false
    }

    fun openExcelAndGetRows(filePath: String, sheetName: String = "Лист1"): MutableIterator<Row> {
        val workbook = XSSFWorkbook(FileInputStream(File(filePath)))
        val sheet = workbook.getSheet(sheetName)
        workbook.close()
        return sheet.iterator()
    }

    fun createTeaPositions(rows: MutableIterator<Row>): MutableList<TeaPosition> {
        val teaPositions = mutableListOf<TeaPosition>()
        try {
            var teaPositionType = TeaType.SHU_PUER_PRESS
            while (rows.hasNext()) {
                val currentRow = rows.next()
                if (currentRow.getCell(5)?.toString().isNullOrEmpty() || currentRow.rowNum == 0)
                    continue
                if (currentRow.getCell(2)?.toString().isNullOrEmpty()) {
                    break
                }
                for (teaType in TeaType.values()) {
                    if (currentRow.getCell(0)?.stringCellValue.toString().contains(teaType.descriptions))
                        teaPositionType = teaType
                }
                if (!currentRow.getCell(6)?.toString().isNullOrEmpty())
                    cost += currentRow.getCell(6).numericCellValue
                val teaPosition = TeaPosition(currentRow.getCell(1).stringCellValue,
                                              currentRow.getCell(2).stringCellValue,
                                              currentRow.getCell(5).numericCellValue,
                                              teaPositionType)
                if (teaPosition.name != "")
                    teaPositions.add(teaPosition)
            }
            return teaPositions
        } catch (e: IllegalStateException){  }
        return teaPositions
    }
}