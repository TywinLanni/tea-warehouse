package com.github.tywinlanny.teawarehouse.logic

import kotlinx.serialization.Serializable

@Serializable
class Settings(val googleDrivePath: String, val ordersPath: String, val takeFromOptSheetName: String)