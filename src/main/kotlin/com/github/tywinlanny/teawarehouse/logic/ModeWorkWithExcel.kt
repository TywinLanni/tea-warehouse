package com.github.tywinlanny.teawarehouse.logic

enum class ModeWorkWithExcel {
    ADD,
    SUBTRACT;
}