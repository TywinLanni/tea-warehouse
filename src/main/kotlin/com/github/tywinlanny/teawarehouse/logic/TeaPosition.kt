package com.github.tywinlanny.teawarehouse.logic

class TeaPosition(val art: String = "", val name: String = "", val value: Double = 0.0, val type: TeaType = TeaType.UNKNOWN) {

    val isPress: Boolean
        get() = art.contains("PEB") || art.contains("PEZ") || art.contains("PEC") || art.contains("BCB")

    private val isPackage: Boolean
        get() = art.contains("PP") || art.contains("386")

    override fun toString() = when {
                this.value == 0.0 ->  "$art | $name \n"
                this.isPress || this.isPackage -> "$art | $name | ${value.toInt()} шт.\n"
                else -> "$art | $name | ${String.format("%.3f", value)} кг.\n"
            }

    override fun equals(other: Any?): Boolean {
        if (other is String)
            return other == art
        if (other is TeaPosition)
            return other.art == art
        return super.equals(other)
    }

    override fun hashCode(): Int {
        return art.hashCode()
    }
}