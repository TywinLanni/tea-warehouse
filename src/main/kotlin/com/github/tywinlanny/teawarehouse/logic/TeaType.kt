package com.github.tywinlanny.teawarehouse.logic

enum class TeaType(val descriptions:  String) {
    SHU_PUER_PRESS("Пуэр Шу пресс"),
    SHEN_PUER_PRESS("Пуэр Шэн пресс"),
    WRITE_PRESS("Белый пресс"),
    SHU_PUER_PLACER("Пуэр Шу россыпь"),
    SHEN_PUER_PLACER("Пуэр Шэн россыпь"),
    RED("Красный"),
    GREEN("Зелёный"),
    WRITE("Белый россыпь"),
    FXDC("Гуандунские улуны"),
    DHP("Северофуцзяньские улуны"),
    TG("Южнофуцзяньские улуны"),
    TAIWAN("Тайваньские улуны"),
    GRASS("Травы"),
    PACKAGE("Пакеты"),
    UNKNOWN("zaleppa");

    override fun toString() = descriptions
}