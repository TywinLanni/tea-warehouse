package com.github.tywinlanny.teawarehouse


class MyException: Exception()

fun riskyCode(number: Int){
    if (number != 10)
        throw MyException()
}

fun main() {
    var i = 0
    while (true){
        try {
            riskyCode(i)
            break
        } catch (e: MyException){
            i++
        } finally {
            println("zaleppa")
        }
        println("zheppa")
    }
    println(i)
}