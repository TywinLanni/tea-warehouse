package com.github.tywinlanny.teawarehouse.ui

import com.github.tywinlanny.teawarehouse.logic.ModeWorkWithExcel
import com.github.tywinlanny.teawarehouse.logic.TeaPosition
import com.github.tywinlanny.teawarehouse.logic.WorkWithExcelFile
import tornadofx.*

class TakeFromOptController: Controller() {

    val roznOrder = mutableListOf<TeaPosition>()
    private val work = WorkWithExcelFile()
    var getToRozn = false

    fun getNameTeaPositionByArt(art: String) = UiHelper.warehouse.first { it.art == art }.name
    
    fun takeFromOpt(){
        work.setOrder(roznOrder)
        roznOrder.clear()
        work.refactorExcelFile(UiHelper.warehousePath.value, ModeWorkWithExcel.SUBTRACT)
        work.refactorExcelFile(UiHelper.takeFromOptPath.value, ModeWorkWithExcel.ADD, UiHelper.takeFromOptSheetName.value)
        if (getToRozn)
            work.refactorExcelFile(UiHelper.roznPath.value, ModeWorkWithExcel.ADD)
    }
}