package com.github.tywinlanny.teawarehouse.ui

import com.github.tywinlanny.teawarehouse.logic.WorkWithExcelFile
import tornadofx.*
import java.io.File
import java.text.SimpleDateFormat
import java.util.*

class IncomeController: Controller() {

    private val work = WorkWithExcelFile()

    private val months = listOf("january", "february", "march", "april", "may", "june", "july", "august", "september", "october", "november", "december")

    fun getIncomeByMonth(): String {
        var allIncome = ""
        for (year in 2019..SimpleDateFormat("yyyy").format(Date()).toInt()) {
            allIncome += "$year год:\n"
            for (month in months) {
                var monthIncome = 0.0
                for (order in File(UiHelper.orderPath.value).walk().filter { !it.isDirectory && it.absolutePath.contains(month) && it.absolutePath.contains(year.toString())}) {
                    work.adaptationOptOrder(order.absolutePath)
                    monthIncome += work.cost
                }
                allIncome += "$month -> $monthIncome руб.\n"
            }
        }
        return allIncome
    }
}