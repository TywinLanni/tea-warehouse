package com.github.tywinlanny.teawarehouse.ui


import tornadofx.*

class IncomeView : View() {
    override val root = hbox {
        prefHeight = 400.0
        prefWidth = 300.0
        paddingAll = 10.0
    }

    private val controller: IncomeController by inject()

    init {
        with(root) {
            scrollpane {
                prefWidth = 300.0
                label(controller.getIncomeByMonth()) {paddingAll = 10.0}
            }
        }
    }

}
