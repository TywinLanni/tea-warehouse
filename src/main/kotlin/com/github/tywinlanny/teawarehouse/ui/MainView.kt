package com.github.tywinlanny.teawarehouse.ui

import javafx.scene.control.*
import javafx.stage.Modality
import javafx.stage.StageStyle
import tornadofx.*
import java.io.File
import java.io.FileNotFoundException
import java.lang.NullPointerException
import java.lang.NumberFormatException


class MainView : View("Warehouse without ceremony") {

    private var ends: Label by singleAssign()
    private var endsPlacerValue: TextField by singleAssign()
    private var endsPressValue: TextField by singleAssign()
    private var orderName: TextField by singleAssign()
    private var orderLabel: Label by singleAssign()
    private var refactorOptButton: Button by singleAssign()
    private var refactorExcelButton: Button by singleAssign()
    private var refactorExcelButtonChina: Button by singleAssign()
    private var listExcel: ListView<String> by singleAssign()

    override val root = vbox {
        minHeight = 600.0
        minWidth = 900.0
        paddingAll = 12.5
        spacing = 12.5
    }

    private val controller: MainWindowController by inject()

    init {
        with(root) {
            hbox {
                paddingAll = 12.5
                spacing = 12.5
                button("Взять из опта") {
                    prefWidth = 90.0
                    action {
                        TakeFromOptView().openWindow(stageStyle = StageStyle.UTILITY, modality = Modality.WINDOW_MODAL, block = true, escapeClosesWindow = false)
                        refreshView()
                    }
                }
                /*button("${controller.getAllIncomeOnAllTime()} руб") {
                    prefWidth = 150.0
                    action {
                        IncomeView().openWindow(stageStyle = StageStyle.UTILITY, modality = Modality.WINDOW_MODAL, block = true, escapeClosesWindow = false)
                    }
                }*/
            }
            hbox {
                paddingAll = 12.5
                spacing = 12.5
                vbox {
                    prefWidth = 300.0
                    label("Заканчивается:")
                    scrollpane {
                        prefHeight = 400.0
                        ends = label()
                    }
                    hbox {
                        label("Преса <")
                        endsPressValue = textfield {
                            text = "7"
                            prefWidth = 40.0
                            textProperty().addListener { _, _, new ->
                                try {
                                    controller.endsPressValue.value = new.toDouble()
                                    ends.text = controller.getEndedTeaPosition()
                                } catch (e: NumberFormatException) {
                                    ends.text = ""
                                }
                            }
                        }
                        label("шт.")
                        label("Россыпи <")
                        endsPlacerValue = textfield {
                            text = "2"
                            prefWidth = 40.0
                            textProperty().addListener { _, _, new ->
                                try {
                                    controller.endsPlacerValue.value = new.toDouble()
                                    ends.text = controller.getEndedTeaPosition()
                                } catch (e: NumberFormatException) {
                                    ends.text = ""
                                }
                            }
                        }
                        label("кг.")
                        spacing = 10.0
                    }
                }
                vbox {
                    prefWidth = 250.0
                    hbox {
                        label("Найденые Excel файлы:")
                        button("обновить") {
                            action {
                                listExcel.items.clear()
                                listExcel.items = File(UiHelper.orderPath.value).list()!!.toList().filter { it.contains(".xlsx") }.toObservable()
                            }
                        }
                    }
                    listExcel = listview<String> {
                        items = File(UiHelper.orderPath.value).list()!!.toList().filter { it.contains(".xlsx") }.toObservable()
                        selectionModel.selectedItemProperty().addListener { _, _, newValue ->
                            orderName.text = newValue
                        }
                        prefHeight = 400.0
                    }
                }
                vbox {
                    prefWidth = 300.0
                    label("Новый заказ")
                    orderName = textfield {
                        prefHeight = 25.0
                        action {
                            controller.orderName.value = text
                            try {
                                orderLabel.text = controller.getNewOrder()
                                refactorOptButton.isDisable = false
                                refactorExcelButton.isDisable = false
                                refactorExcelButtonChina.isDisable = false
                            } catch (e: FileNotFoundException) {
                                refactorOptButton.isDisable = true
                                refactorExcelButton.isDisable = true
                                refactorExcelButtonChina.isDisable = true
                            } catch (e: NullPointerException) {
                                refactorExcelButtonChina.isDisable = false
                            }
                        }
                    }
                    scrollpane {
                        prefHeight = 400.0 - orderName.prefHeight
                        orderLabel = label()
                    }
                    hbox {
                        refactorOptButton = button("Вычесть из опта") {
                            isDisable = true
                            action {
                                confirm("Вы уверены?",
                                        "",
                                        ButtonType.YES,
                                        ButtonType.NO) {
                                    controller.refactorOptFile()
                                    refreshView()
                                    isDisable = true
                                }
                            }
                        }
                        refactorExcelButton = button ("Преобразовать") {
                            isDisable = true
                            action {
                                controller.refactorExcelToMyWarehouseFormat()
                                information("Файл успешно преобразован") {}
                                isDisable = true
                            }
                        }
                        refactorExcelButtonChina = button("Китай"){
                            isDisable = true
                            action {
                                if (controller.refactorChinaFile()) {
                                    information("Файл успешно преобразован") {}
                                    isDisable = true
                                }
                            }
                        }
                    }
                }
            }
        }
        controller.orderName.value = orderName.text
        controller.endsPlacerValue.value = endsPlacerValue.text.toDouble()
        controller.endsPressValue.value = endsPressValue.text.toDouble()
        refreshView()
    }

    private fun refreshView(){
        try {
            controller.loadWarehouse()
            ends.text = controller.getEndedTeaPosition()
        }catch (e: FileNotFoundException){
            ends.text = "не найден оптовый файл"
        }
    }
}
