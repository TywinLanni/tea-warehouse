package com.github.tywinlanny.teawarehouse.ui

import com.github.tywinlanny.teawarehouse.logic.TeaPosition
import javafx.scene.control.TableView
import javafx.scene.control.TextField
import tornadofx.*
import java.lang.NumberFormatException

class TakeFromOptView : View("Взяли из опта") {

    private var art: TextField by singleAssign()
    private var name: TextField by singleAssign()
    private var value: TextField by singleAssign()
    private var orderTable: TableView<TeaPosition> by singleAssign()

    override val root = hbox {
        prefHeight = 300.0
        prefWidth = 780.0
        spacing = 12.5
        paddingAll = 12.5
    }

    private val controller: TakeFromOptController by inject()

    init {
        with(root){
            vbox {
                hbox {
                    label("Артикул").prefWidth = 70.0
                    label("Наименование").prefWidth = 250.0
                    label("Кол-во").prefWidth = 50.0
                    spacing = 15.0
                    paddingHorizontal = 12.5
                }
                hbox {
                    art = textfield {
                        tooltip("Артикул")
                        prefWidth = 70.0
                        textProperty().addListener { _, _, new ->
                            try { name.text = controller.getNameTeaPositionByArt(new.toUpperCase()) } catch (e: NoSuchElementException) { }
                        }
                    }
                    name = textfield {
                        tooltip("Наименование")
                        prefWidth = 250.0
                    }
                    value = textfield {
                        filterInput { it.controlNewText.isDouble() }
                        tooltip("Кол-во")
                        prefWidth = 50.0
                    }
                    button("Добавить") {
                        action {
                            try {
                                controller.roznOrder.add(TeaPosition(art.text.toUpperCase(), name.text, value.text.toDouble()))
                                orderTable.items.clear()
                                orderTable.items.addAll(controller.roznOrder)
                                art.clear()
                                name.clear()
                                value.clear()
                            } catch (e: NumberFormatException) {
                                warning("", "В поле Кол-во должно быть число!")
                            }
                        }
                    }
                    checkbox("Добавить в Розницу") { action { controller.getToRozn = isSelected } }
                    spacing = 12.5
                    paddingAll = 12.5
                }
                orderTable = tableview {
                    readonlyColumn("Art", TeaPosition::art){ prefWidth = 90.0 }
                    readonlyColumn("Name", TeaPosition::name) { prefWidth = 265.0 }
                    readonlyColumn("Value", TeaPosition::value){ prefWidth = 60.0 }
                }
            }
            vbox {
                button("Взять") {
                    action {
                        controller.takeFromOpt()
                        orderTable.items.clear()
                    }
                }
                spacing = 325.0
                paddingAll = 12.5
                }
            }
        }

    override fun onDock() {
        currentWindow?.setOnCloseRequest {
            controller.roznOrder.clear()
        }
    }
}
