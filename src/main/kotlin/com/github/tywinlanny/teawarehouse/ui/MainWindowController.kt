package com.github.tywinlanny.teawarehouse.ui

import com.github.tywinlanny.teawarehouse.logic.ModeWorkWithExcel
import com.github.tywinlanny.teawarehouse.logic.TeaPosition
import com.github.tywinlanny.teawarehouse.logic.WorkWithExcelFile
import javafx.beans.property.SimpleDoubleProperty
import javafx.beans.property.SimpleStringProperty
import tornadofx.*
/*import java.io.File
import java.io.FileNotFoundException*/


class MainWindowController: Controller() {

    val orderName = SimpleStringProperty()
    private val work = WorkWithExcelFile()
    private val warehouse = mutableListOf<TeaPosition>()
    val endsPlacerValue = SimpleDoubleProperty()
    val endsPressValue = SimpleDoubleProperty()

    fun getNewOrder(): String = work.adaptationOptOrder(UiHelper.orderPath.value + "/" + orderName.value).joinToString("") + "\n стоимость ${work.cost}"

    //fun getNotAvailableTeaPosition(): String = warehouse.filter { it.value == 0.0 }.joinToString("")

    fun getEndedTeaPosition(): String = warehouse.filter { it.value != 0.0 }
                            .filter { it.isPress && it.value < endsPressValue.value || !it.isPress &&  it.value < endsPlacerValue.value }
                            .joinToString("")

    fun refactorExcelToMyWarehouseFormat() = work.refactorExcelFileToMyWarehouse(UiHelper.orderPath.value + "/" + orderName.value)

    fun loadWarehouse() {
        warehouse.clear()
        UiHelper.warehouse.clear()
        warehouse.addAll(work.createTeaPositions(work.openExcelAndGetRows(UiHelper.warehousePath.value)))
        UiHelper.warehouse.addAll(warehouse)
    }

    fun refactorOptFile() {
        work.refactorExcelFile(UiHelper.warehousePath.value, ModeWorkWithExcel.SUBTRACT)
    }

    /*fun getAllIncomeOnAllTime(): Double{
        var allIncome = 0.0
        for (order in File(UiHelper.orderPath.value).walk().filter { !it.isDirectory }) {
            work.adaptationOptOrder(order.absolutePath)
            allIncome += work.cost
        }
        return allIncome
    }*/

    fun refactorChinaFile(): Boolean = work.refactorChinaPrice(UiHelper.orderPath.value + "/" + orderName.value, orderName.value)
}
