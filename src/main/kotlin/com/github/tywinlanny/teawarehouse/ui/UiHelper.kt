package com.github.tywinlanny.teawarehouse.ui

import com.github.tywinlanny.teawarehouse.logic.Settings
import com.github.tywinlanny.teawarehouse.logic.TeaPosition
import javafx.beans.property.SimpleStringProperty
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.JsonConfiguration
import java.io.File
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

object UiHelper {

    val warehousePath = SimpleStringProperty()
    val takeFromOptPath = SimpleStringProperty()
    val takeFromOptSheetName = SimpleStringProperty()
    val orderPath = SimpleStringProperty()
    val warehouse = mutableListOf<TeaPosition>()
    val roznPath = SimpleStringProperty()
    val oldWarehousePath = SimpleStringProperty()

    init {
        val calendar = Calendar.getInstance()
        calendar.add(Calendar.DATE, -1)
        val dateFormat = SimpleDateFormat("dd_MMMM_yyyy")
        val settingsString = File(File("").absolutePath + "/settings.json").readText()
        val settings = Json(JsonConfiguration.Stable).parse(Settings.serializer(), settingsString)
        warehousePath.value = settings.googleDrivePath + "/" + "STOCK_SPB_WH_${dateFormat.format(Date())}.xlsx"
        oldWarehousePath.value = settings.googleDrivePath + "/" + "STOCK_SPB_WH_${dateFormat.format(calendar.time)}.xlsx"
        takeFromOptPath.value = settings.googleDrivePath + "/" + "Взяли из опта.xlsx"
        roznPath.value =  settings.googleDrivePath + "/" + "Склад Розница ЧБЦ.xlsx"
        takeFromOptSheetName.value = settings.takeFromOptSheetName
        orderPath.value = settings.ordersPath
    }

}